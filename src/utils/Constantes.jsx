export const ROUTE = {
	URL_PATH: '/',
	URL_FORMULARIO: '/formulario',
}

/* PRODUCCIÓN */
const DATAPOWER = "https://"

/** DESARROLLO */
const APICLOUD = "https://"


export const API = {
    URL_CONSULTA_POSTULANTES	: APICLOUD+"consultapostulantes",
    URL_GUARDAR_POSTULANTES	    : APICLOUD+"guardarpostulantes",
}
