import React, { Component } from 'react';
// import { connect } from 'react-redux';
// import {ROUTE} from '../utils/Constantes'

//importar páginas
import InfoPersonal from '../components/forms/InfoPersonal'
import ExpLaboral from '../components/forms/ExpLaboral'
import SuccessRegister from '../components/registro/SuccessRegister'
//importar componentes
 

class Formulario extends Component {

	constructor(props){
		super(props)
		this.state = {
            isForm1: true, // Form Información Personal
            isForm2: false, // Form Experiencia Laboral Externa
            isPage2: false // Registro Existoso
		}
	}


	handlerPaso = (paso) => {

		if(paso == 9){
			for (let i = 1; i <= 9; i++) {
				this.setState({
					["isForm" + i]: false
				})
			}
			return;
        }
        

		for (let i = 1; i <= 9; i++) {
			if (i === paso) {
				this.setState({
					["isForm" + i]: true
				})
			} else {
				this.setState({
					["isForm" + i]: false
				})
			}
		}
	}


	render() {
		let { isForm1, isForm2, isPage2} = this.state
		 
		return (
			<section className="bxPge">
				<section className="container">
                        {isForm1 ? <InfoPersonal handlerPaso={this.handlerPaso} /> : null}
                        {isForm2 ? <ExpLaboral handlerPaso={this.handlerPaso} /> : null}
						{isPage2 ? <SuccessRegister handlerPaso={this.handlerPaso} /> : null}
					</section>
					 
				</section>
		)
	}



}


export default Formulario