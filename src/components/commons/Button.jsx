import React from 'react'

const Button = ({handlerOnClick, clase, value}) =>{
    return(
        <button className={clase} onClick={()=>handlerOnClick()}>{value}</button> 
    )
}
export default Button