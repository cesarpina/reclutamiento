import React from 'react'

const Title = ({clase, value}) =>{
    return(
        <h1 className={`title `+clase}>{value}</h1> 
    )
}
export default Title