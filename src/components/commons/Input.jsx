import React from 'react';

const Input = ({ clase, name, value, handlerOnChange, holder, disableCampo, longitud ,minlog }) => (
	<div className={"input-field input "+clase}>
		<input
			type='text'
			className={"validate mb-full cienp"}
			id={name}
			name={name}
			value={value}
			maxLength={longitud}
			minLength={minlog}
			disabled={disableCampo}
			onChange={(e) => handlerOnChange(e)} /> 
			
			<label id={"label-"+name} htmlFor={name} className={"label-input"+( value?" active":"" )} >{holder}</label>
	</div>
)


export default Input