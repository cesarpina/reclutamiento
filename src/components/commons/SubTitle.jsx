import React from 'react'

const SubTitle = ({clase, value}) =>{
    return(
        <h1 className={`subTitle `+clase}>{value}</h1> 
    )
}
export default SubTitle