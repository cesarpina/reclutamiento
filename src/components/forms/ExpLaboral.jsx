import React, { Component } from 'react';
import Button from '../commons/Button';
import Input from '../commons/Input';
import {Input as InputMaterialize} from 'react-materialize';
import SubTitle from '../commons/SubTitle';

class ExpLaboral extends Component {

        constructor(props){
            super(props)
            this.state = {
                var: ''

            }
        }

        pasoSiguiente = () =>{
            this.props.handlerPaso(2);
        }


render() {

    return(
        <section className="bxWhite d-flex flex-wrap bounceInLeft animated">
            <div className="bxForm bxForm3 d-flex flex-wrap justify-content-between">
                <SubTitle value="Experiencia Laboral" />
                <Input
                    clase="inputText mb-full"
                    name="apellido-paterno"
                    holder="Apellido paterno"
                    handlerOnChange={this.handlerOnChange}
                    longitud="20"
                />
                <Input
                    clase="inputText mb-full"
                    name="apellido-materno"
                    holder="Apellido Materno"
                    handlerOnChange={this.handlerOnChange}
                    longitud="20"
                /> 
                <Input
                    clase="inputText mb-full"
                    name="nombres"
                    holder="Nombres"
                    handlerOnChange={this.handlerOnChange}
                    longitud="20"
                />

                    <InputMaterialize s={12} type='select' label="Género" defaultValue='1'>
                        <option value='1'>Hombre</option>
                        <option value='2'>Mujer</option>
                    </InputMaterialize>
            </div>
            <div className="full d-flex justify-content-center">
                <Button clase="btnSucces z-depth-1 mb-full waves-effect " 
                    value="SIGUIENTE" 
                    handlerOnClick={() => this.pasoSiguiente()} />
            </div>
        </section>
    )
        }           
}


export default ExpLaboral
 