import React, { Component } from 'react';
import Button from '../commons/Button';
import Title from '../commons/Title';
import SubTitle from '../commons/SubTitle'
import Input from '../commons/Input';
import {Input as InputMaterialize} from 'react-materialize';

const optionCalendar = {

    // min: ,
    // max: ,
    today: 'Hoy',
    clear: 'Limpiar',
    close: 'OK',
    format: 'dd/mm/yyyy',
    closeOnSelect: false,
    monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
    selectYears: true,
    selectMonths: true,

};

class InfoPersonal extends Component {

        constructor(props){
            super(props)
            this.state = {
                var: ''

            }
        }

        

        pasoSiguiente = () =>{
            this.props.handlerPaso(2);
        }


render() {

    return(
        <section className="bxWhite d-flex justify-content-center flex-wrap">
            <Title value="REGISTRO DE POSTULANTES" />
            
            <div className="bxForm bxForm3 d-flex flex-wrap justify-content-between">
                <SubTitle value="Información Personal" />

                <InputMaterialize s={12} type='select' label="Tipo Documento" defaultValue='1' disabled>
                    <option value='1'>DNI</option>
                    <option value='2'>CE</option>
                </InputMaterialize>

                <Input
                    clase="inputText mb-full"
                    name="documento"
                    holder="Número de Documento"
                    handlerOnChange={this.handlerOnChange}
                    longitud="20"
                    disableCampo="disabled"
                />

                <InputMaterialize type='select' label="Género" defaultValue='1'>
                    <option value='1'>Hombre</option>
                    <option value='2'>Mujer</option>
                </InputMaterialize>


                <Input
                    clase="inputText mb-full"
                    name="apellido-paterno"
                    holder="Apellido paterno"
                    handlerOnChange={this.handlerOnChange}
                    longitud="20"
                />
                <Input
                    clase="inputText mb-full"
                    name="apellido-materno"
                    holder="Apellido Materno"
                    handlerOnChange={this.handlerOnChange}
                    longitud="20"
                />
                <Input
                    clase="inputText mb-full"
                    name="nombres"
                    holder="Nombres"
                    handlerOnChange={this.handlerOnChange}
                    longitud="20"
                />

                

                <InputMaterialize name='on' type='date' label="Fecha de Nacimiento" className="datepicker" options={optionCalendar} onChange={function(e, value) {}} />

                

            </div>




            <div className="full d-flex justify-content-center">
                <Button clase="btnSucces z-depth-1 mb-full waves-effect " 
                    value="SIGUIENTE" 
                    handlerOnClick={() => this.pasoSiguiente()} />
            </div>
            
        </section>
    )
        }           
}


export default InfoPersonal
 