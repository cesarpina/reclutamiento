import React, { Component } from 'react';
// import { Provider } from 'react-redux';
import { BrowserRouter as Router, Redirect, Route } from 'react-router-dom';
import { ROUTE } from './utils/Constantes'
import Formulario from './containers/Formulario';

import 'jquery'
import './../node_modules/materialize-css/dist/js/materialize.min.js'
import './../node_modules/materialize-css/dist/css/materialize.min.css'

import './resources/css/transitions.css';
import './resources/css/base.css';
import './resources/css/fonts.css';
require('materialize-css');

class App extends Component {
	render() {
		return (
		//	<Provider>
				<Router basename={ROUTE.URL_PATH}>
					<div>
						<Route exact path="/" render={() => (<Redirect to={ROUTE.URL_FORMULARIO} />)} ></Route>
						<Route exact path={ROUTE.URL_FORMULARIO} component={Formulario} ></Route>
					</div>
				</Router>
		//	</Provider>
		);
	}
}

export default App;